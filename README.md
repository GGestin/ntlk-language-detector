# NTLK Language Detector
##### Gaëtan GESTIN, @11/2022


This project was an assignement for my **Natural Languages** courses


The assignment consists in the development, in NLTK,
OpenNLP, SketchEngine or GATE/Annie a Naïve Bayes Classifier able to detect a single class in
one of the corpora available as attachments to the chosen package, by distinguishing ENGLISH
against NON-ENGLISH. In particular the classifier has to be:

   1. Trained on a split subset of the chosen corpus, by either using an existing partition between
       sample documents for training and for test or by using a random splitter among the available
       ones;

   2. Devised as a pipeline of any chosen format, including the simplest version based on word2vec
       on a list of words obtained by one of the available lexical resources.

The test of the classifier shall give out the measures of accuracy, precision, recall on the ob-
tained confusion matrix and WILL NOT BE EVALUATED ON THE LEVEL OF THE PERFOR-
MANCES. In other terms, when the confusion is produced, then the value of the assignment will
be good, independently of the percentage of false positive and negative results.

Deliver a short set of comments on the experience (do not deliver the entire code, but link it
on a public repository like GitHub or the GATE Repo). Discuss: size of the corpus, size of the
split training and test sets, performance indicators employed and their nature, employability of the
classifier as a Probabilistic Language Model.

First assignment delivery is delayed to the end of November to allow for discussion in the remainder
period.
